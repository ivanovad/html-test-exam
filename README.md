# Тест

1. Каким цветом будет выведен text1, а каким text2?
  ```html
    <div class="red blue">text 1</div>
    <div class="blue red">text 2</div>
  ```

  ```css
    .red { color: red; }
    .blue { color: blue; }
  ```

  **Ответ:** [text1 - blue, text2 - blue]

2. Какого цвета будут животные?
  2.1. Дополнительный вопрос: что бы вы исправили в коде, валидный ли он?

  ```html
    <div class="body">
      <ul class="list">
        <li id="elephants">Слоны</li>
        <li id="tigers">Тигры</li>
        <li id="dogs" style="color: blue;">Собаки</li>
      </ul>
    </div>
  ```

  ```css
    .body {
      display: block;
      color: blue;
    }

    .body li { color: red; }
    #elephants { color: yellow; }

    .list {
      line-height: 10px;
      width: 100%;
    }

    .list [id="elephants"] { color: green; }
  ```

  **Ответ:** [Слоны - жёлтый, тигры - красный, собаки - синий. Последнее css правило не выполняется ни для какого элемента, потому что квадратные скобки - обращение к атрибуту, а у ul атрибутов никаких нет. Так что данное правило нужно убрать или изменить (.list #elephants)]

3. Валиден ли данный код? Объясните свой ответ.

  ```html
    <div>
      <a href="/">
        <span>
          Контур
          <a href="/1.html">1.html</a>
        </span>
      </a>
    <div>
  ```

  **Ответ:** [Так как внутренняя ссылка находится внутри другой ссылки, то сработает в итоге внешняя. То есть по клику на "1.html" мы получим не то, что ожидали и окажемся в корне сайта, а не на 1.html]
    
4. Какой размер line-height в пикселях будет у текста?
  ```css
    p {
      height: 20px;
      font-size: 16px;
      line-height: 2.25;
    }
  ```

  **Ответ:** [36px]
    
5. Чему равен размер font-size у h1 в пикселях?
  ```html
    <body>
      <div>
        <h1>Заголовок</h1>
      </div>
    </body>
  ```

  ```css
    html { font-size: 16px; }
    div { font-size: 18px; }
    div h1 { font-size: 2rem; }
  ```

  **Ответ:** [32px]


6. Подсчитать размеры зеленого прямоугольника (высоту и ширину)
  ```html
  <div class="wrapper">
    <div class="block">text</div>
  </div>
  ```
  ```css
    .wrapper { width: 600px; }
    .block {
      background: green;
      padding: 10% 15%;
      width: 40%;
      line-height: 1;
      font-size: 16px;
    }
  ```

  **Ответ:** [высота: 136px, ширина: 420px]


7. Нарисуйте и опишите что будет выведено в браузере

  ```html
    <div class="wrapper">
      <div class="inner"></div>
    </div>
  ```

  ```css
    body { margin: 0; }

    .wrapper {
      position: relative;
      top: 20px;
      left: 20px;
      padding: 10px;
      margin: 10px;
      width: 100px;
      height: 100px;
      border: 1px solid green;
      box-sizing: border-box;
    }

    .inner {
      position: absolute;
      top: 20px;
      left: 20px;
      width: 10px;
      height: 10px;
      border: 1px solid red;
      box-sizing: border-box;
    }
  ```

  **Ответ:** [Внутренний блок позиционировался абсолютно относительно своего родителя с отсупами сверху и слева (сразу после паддингов родителя). А родительский блок спозиционировлся относительно своего начального расположения с отсупами ]
  
8.  Какой ширины будут block1, block2 и block3?

  ```html
    <div class="wrapper">
      <div class="block1"></div>
      <div class="block2"></div>
      <div class="block3"></div>
    </div>
  ```

  ```css
    .wrapper {
      display: flex;
      width: 300px;
    }

    .block1 {
      flex-grow: 2;
      width: 100px;
    }

    .block2 {
      flex-grow: 1;
      width: 75px;
    }

    .block3 {
      flex-grow: 2;
      width: 25px;
    }
  ```

  **Ответ:** [block1: 166.66px, block2: 108.33px, block3: 91.66px]

9. Необходимо реализовать галерею фотографий плиткой. Есть готовая разметка и стили на элементы. Нужно дописать стили галереи так, чтобы получилось как на рисунке.

Примечание: Ожидается решение на flex, но можете попробовать и другие варианты. Стили для .item менять нельзя.

  ![галерея](./img/gallery.png)


  ```html
    <div class="gallery">
        <div class="item"></div>
        <div class="item"></div>
        <div class="item"></div>
        <div class="item _big"></div>
        <div class="item"></div>
        <div class="item"></div>
        <div class="item _big"></div>
    </div>
  ```

  ```css
    .galery{
      display: flex;
      flex-wrap: wrap;
      justify-content: space-between;
      align-items: center;
    }

    .item {
      width: 80px;
      height: 40px;
      margin: 10px;
      border: 2px solid indigo;
      box-sizing: border-box;
    }

    .item._big {
      width: 180px;
      height: 80px;
    }
  ```

  **Ответ:** [Добавлены стили для класса galery]

В следующих заданиях необходимо нарисовать, что будет выведено в браузере. Можно рисовать в любом графическом редакторе. Получившиеся картинки нужно сохранить в папке img/задание_(номер).png

10.   Нарисуйте и опишите что будет выведено в браузере

  ```html
    <div class="wrapper1">
        <div class="block1_1"></div>
    </div>
    <div class="wrapper2">
      <div class="block2_1"></div>
    </div>
  ```

  ```css
    /* Задание A. Что будет выведено в браузере? */
    .wrapper1 {
      position: relative;
      z-index: 1;
      width: 50px;
      height: 50px;
      border: 1px solid red;
    }

    .wrapper2 {
      position: relative;
      z-index: 2;
      width: 50px;
      height: 50px;
      border: 1px solid green;
    }

    /* Задание B. Что будет выведено в браузере, если добавить этот код? */
    .block1_1 {
      position: absolute;
      bottom: -25px;
      z-index: 10;
      width: 50px;
      height: 50px;
      background: red;
    }

    .block2_1 {
      position: absolute;
      top: -25px;
      z-index: 5;
      width: 50px;
      height: 50px;
      background: green;
    }
  ```

  **Ответ:** [В задании А белые отсупы сверху и слева - стандартный margin у body.]
  
11.   Нарисуйте и опишите что будет выведено в браузере

  ```html
    <div class="container">
      <div id="item-1" class="item item-1">1</div>
      <div id="item-2" class="item item-2">2</div>
      <div id="item-3" class="item item-3">3</div>
      <div id="item-4" class="item item-4">4</div>
      <div id="item-5" class="item item-5">5</div>
      <div id="item-6" class="item item-6">6</div>
    </div>
  ```

  ```css
    /* Задание A. Что будет выведено в браузере? */
    .container {
      position: relative;
      box-sizing: border-box;
      padding: 15px;
      border: 5px solid grey;
      width: 400px;
      display: inline-block;
    }
    .item {
        float: left;
        width: 50px;
        height: 50px;
        margin: 5px;
        background-color: red;
        color: white;
        line-height: 50px;
        text-align: center;
    }

    /* Задание B. Что будет выведено в браузере, если добавить этот код? */
    .item::after {content: '_item'}
    div.item:first-child,
    div.item:last-child { background-color: blue; }

    /* Задание C. Что будет выведено в браузере, если добавить этот код? */
    .item-2 + .item { visibility: hidden; }
    .item-5 - .item { background-color: yellow; }
  ```

  **Ответ:** [...]
